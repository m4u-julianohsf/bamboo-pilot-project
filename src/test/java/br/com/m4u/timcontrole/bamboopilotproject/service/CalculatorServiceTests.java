package br.com.m4u.timcontrole.bamboopilotproject.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

/**
 * Created by julianofernandes on 01/06/17.
 */

@RunWith(JUnit4.class)
public class CalculatorServiceTests {

    private CalculatorService service;

    @Before
    public void setUp() {
        service = new CalculatorService();
    }

    @Test
    public void sum3And4() throws Exception {
        Integer result = service.sum(3, 4);
        assertEquals(Integer.valueOf(7), result);
    }

}
