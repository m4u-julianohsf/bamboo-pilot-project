package br.com.m4u.timcontrole.bamboopilotproject.service;

import org.springframework.stereotype.Service;

/**
 * Created by julianofernandes on 01/06/17.
 */
@Service
public class CalculatorService {

    public Integer sum(Integer factor1, Integer factor2) {
        if (factor1 != null && factor2 != null) {
            return factor1 + factor2;
        }

        if (factor1 == null) {
            return factor2 == null ? 0 : factor2;
        }

        return factor1 == null ? 0 : factor1;
    }
}
