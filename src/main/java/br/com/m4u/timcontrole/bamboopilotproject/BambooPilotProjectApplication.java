package br.com.m4u.timcontrole.bamboopilotproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BambooPilotProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(BambooPilotProjectApplication.class, args);
	}
}
