FROM java:8

ADD pilot-project-*.jar pilot-project.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/pilot-project.jar"]
